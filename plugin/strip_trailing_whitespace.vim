"
" strip_trailing_whitespace.vim: User command to strip both horizontal and
" vertical whitespace in a buffer, with optional range, reporting both
" accurately and restoring the cursor afterwards.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_strip_trailing_whitespace') || &compatible
  finish
endif
if v:version < 700
  finish
endif
let loaded_strip_trailing_whitespace = 1

" User command for the above
command! -bar -range=% StripTrailingWhitespace
      \ call strip_trailing_whitespace#(<line1>, <line2>)
