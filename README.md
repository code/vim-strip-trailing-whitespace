strip\_trailing\_whitespace.vim
===============================

This plugin provides a user command with the author's approach to stripping
trailing whitespace from an entire buffer, including removing empty or
whitespace-only lines at the end of the buffer, without making command noise
and without moving the cursor from its current position.

This is a very commonly written and implemented plugin, but I wrote my own
because I could not find a plugin that did this in exactly the way I wanted:

- Accept an optional range
- Strip trailing lines as well as trailing spaces
- Report what was changed, accurately
- Work with old Vim (>=7.0)
- Work with a single `undo`
- Don't move the cursor
- Don't change the search pattern
- Don't define an `autocmd`
- Don't force a key mapping
- Don't define a global function

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
